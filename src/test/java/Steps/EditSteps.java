package Steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class EditSteps{
	public ChromeDriver driver;
	@Given("launch the browser")
	public void launchBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		 driver = new ChromeDriver();
		 }
	@Given("load the url")
	public void loadTheUrl() {
		driver.get("http://leaftaps.com/opentaps/control/main");
	   }
     @Given("max the browser")
	public void maxTheBrowser() {
		driver.manage().window().maximize();
	    }
   @Given("set the timeouts")
	public void setTheTimeouts() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    }
@Given("enter the username")
	public void enterTheUsername() {
		WebElement username = driver.findElementById("username");
		username.sendKeys("DemoCSR");
	    }
@Given("enter the password")
	public void enterThePassword() {
		driver.findElementById("password").sendKeys("crmsfa");
	}
@When("click on the login button")
	public void clickOnTheLoginButton() {
		driver.findElementByClassName("decorativeSubmit").click();
	}
	@Given("click on the CRM\\/SFA button")
	public void clickOnTheCRMSFAButton() {
		driver.findElementByLinkText("CRM/SFA").click();
	}
	@Given("click on the leads link")
	public void clickOnTheLeadsLink() {
		driver.findElementByLinkText("Leads").click();
	}
  @Given("click on the find leads")
	public void clickOnTheFindLeads() {
	  driver.findElementByLinkText("Find Leads").click();
	}
  @Given("enter the first name")
 	public void enterTheFirstName() {
	  driver.findElementByXPath("//*[@id='ext-gen248']").sendKeys("jagan");
  }

	@Given("click on the find leads button")
	public void clickOnTheFindLeadsButton() {
		driver.findElementByXPath("//*[@id='ext-gen334']").click();
	}

	@Given("click on first resulting lead")
	public void clickOnFirstResultingLead() throws InterruptedException {
		Thread.sleep(3000);
		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-firstName']//a[1]").click();
	}

	@Given("verify the tiitle page")
	public void verifyTheTiitlePage() {
		String vName1 = driver.findElementById("sectionHeaderTitle_leads").getText();
		if(vName1.equals(vName1)) {
			System.out.println("sucessfully verified");
		}else
		{
			System.out.println("not verified");
		}
	}

	@Given("click on the edit")
	public void clickOnTheEdit() {
		driver.findElementByXPath("//*[@id='center-content-column']/div[2]/div[1]/div[1]/div/div/div/div[2]/a[3]").click();
	}

	@Given("change the company name")
	public void changeTheCompanyName() {
		driver.findElementById("updateLeadForm_companyName").clear();
		driver.findElementById("updateLeadForm_companyName").sendKeys("Tata");
	}

	@Given("click on the update")
	public void clickOnTheUpdate() {
		driver.findElementByClassName("smallSubmit").click();
	}

	@Then("confirm the changed name appears")
	public void confirmTheChangedNameAppears() {
	    System.out.println("verified sucessfully");
	}


}
