package Steps;

import java.util.List;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class LoginSteps {
	public ChromeDriver driver;
	@Given("launch the browser")
	public void launchBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		 driver = new ChromeDriver();
		 }
	@Given("load the url")
	public void loadTheUrl() {
		driver.get("http://leaftaps.com/opentaps/control/main");
	   }
     @Given("max the browser")
	public void maxTheBrowser() {
		driver.manage().window().maximize();
	    }
   @Given("set the timeouts")
	public void setTheTimeouts() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    }
@Given("enter the username")
	public void enterTheUsername() {
		WebElement username = driver.findElementById("username");
		username.sendKeys("DemoCSR");
	    }
@Given("enter the password")
	public void enterThePassword() {
		driver.findElementById("password").sendKeys("crmsfa");
	}
@When("click on the login button")
	public void clickOnTheLoginButton() {
		driver.findElementByClassName("decorativeSubmit").click();
	}
	@Given("click on the CRM\\/SFA button")
	public void clickOnTheCRMSFAButton() {
		driver.findElementByLinkText("CRM/SFA").click();
	}

	@Given("click on the leads button")
	public void clickOnTheLeadsButton() {
		driver.findElementByLinkText("Leads").click();
	}
@Given("click on the create lead")
	public void clickOnTheCreateLead() {
		driver.findElementByLinkText("Create Lead").click();
	}
@Given("enter the company name")
	public void enterTheCompanyName() {
		driver.findElementById("createLeadForm_companyName").sendKeys("tnpl");
	}
@Given("enter the first name")
	public void enterTheFirstName() {
		driver.findElementById("createLeadForm_firstName").sendKeys("naga");
	}
@Given("enter the last name")
	public void enterTheLastName() {
		driver.findElementById("createLeadForm_lastName").sendKeys("mani");
	}
@Given("enter the first name local")
	public void enterTheFirstNameLocal() {
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("sel");
		}
@Given("enter the last name local")
	public void enterTheLastNameLocal() {
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("ars");
	}
@Given("enter the salutations")
	public void enterTheSalutations() {
		driver.findElementById("createLeadForm_personalTitle").sendKeys("D");
	}
@Given("choose the source")
	public void chooseTheSource() {
		WebElement eleSource = driver.findElementById("createLeadForm_dataSourceId");
		Select eleDropDown = new Select(eleSource);
		eleDropDown .selectByVisibleText ("Employee");
		}
@Given("enter the tittle")
	public void enterTheTittle() {
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Engineering");
	}
@Given("enter the annual revenue")
	public void enterTheAnnualRevenue() {
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("178000");
	}
@Given("choose  industry")
	public void chooseIndustry() {
		WebElement eleSource2 = driver.findElementById("createLeadForm_industryEnumId");
		Select eleDropDown2 =new Select(eleSource2);
		eleDropDown2 .selectByIndex(3);
	}
	@Given("choose ownership")
	public void chooseOwnership() {
		WebElement eleSource3 = driver.findElementById("createLeadForm_ownershipEnumId");
		Select eleDropDown3 = new Select(eleSource3);
		eleDropDown3.selectByVisibleText("Corporation");
	}
@Given("enter the SIC code")
	public void enterTheSICCode() {
		driver.findElementById("createLeadForm_sicCode").sendKeys("639113");
	}
@Given("enter the description")
	public void enterTheDescription() {
		driver.findElementById("createLeadForm_description").sendKeys("all the employees are asked to atend the meeting without fail");
	}

	@Given("enter the important notes")
	public void enterTheImportantNotes() {
		driver.findElementById("createLeadForm_importantNote").sendKeys("every one should attend without fail");
	}

	@Given("enter the country code")
	public void enterTheCountryCode() {
		driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("600100");
	}

	@Given("enter the area code")
	public void enterTheAreaCode() {
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("04324");
	}

	@Given("enter the extensions")
	public void enterTheExtensions() {
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("271361");
	}

	@Given("enter the department")
	public void enterTheDepartment() {
		driver.findElementById("createLeadForm_departmentName").sendKeys("informationtech");
	}

	@Given("choose prefferd currency")
	public void choosePrefferdCurrency() {
		WebElement eleSource4 = driver.findElementById("createLeadForm_currencyUomId");
		Select eleDropDown4 =new Select(eleSource4);
		List<WebElement> allOptions = eleDropDown4.getOptions();
		int size = allOptions.size();
        eleDropDown4.selectByIndex(size-1);
	}

	@Given("enter the number of employees")
	public void enterTheNumberOfEmployees() {
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("450");
	}

	@Given("enter the ticker symbol")
	public void enterTheTickerSymbol() {
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("@#");
	}
   @Given("enter web url")
	public void enterWebUrl() {
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("http://");
	}

	@Given("enter the address line {int}")
	public void enterTheAddressLine(Integer int1) {
		 driver.findElementById("createLeadForm_generalAddress1").sendKeys("24/A");
	}

	@Given("enter the city")
	public void enterTheCity() {
		driver.findElementById("createLeadForm_generalCity").sendKeys("karur");
	}

	@Given("choose state")
	public void chooseState() {
		 WebElement eleSource5 = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
	        Select eleDropDown5 = new Select(eleSource5);
	       eleDropDown5.selectByValue("FL");
	}
   @Given("enter the zip postal code")
	public void enterTheZipPostalCode() {
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("639113");
	}

	@Given("enter the phone number")
	public void enterThePhoneNumber() {
		 driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("8870430819");
	}

	@Given("enter the email address")
	public void enterTheEmailAddress() {
		 driver.findElementById("createLeadForm_primaryEmail").sendKeys("mani.naga89@gmail.com");
	}
}
