Feature: leaftaps edit testcases

Scenario: positive edit flow
Given launch the browser
And load the url
And max the browser
And set the timeouts
And enter the username
And enter the password
And click on the login button
And click on the CRM/SFA button
And click on the leads link
And click on the find leads
And enter the first name
And click on the find leads button
And click on first resulting lead
And verify the tiitle page
And click on the edit 
And change the company name 
And click on the update
Then confirm the changed name appears