Feature: leaftaps login testcases

Scenario: positive login flow
Given launch the browser
And load the url
And max the browser
And set the timeouts
And enter the username
And enter the password
And click on the login button
And click on the CRM/SFA button
And click on the leads button
And click on the create lead
And enter the company name
And enter the first name
And enter the last name
And enter the first name local
And enter the last name local
And enter the salutations 
And choose the source
And enter the tittle
And enter the annual revenue
And choose  industry
And choose ownership
And enter the SIC code
And enter the description
And enter the important notes
And enter the country code
And enter the area code
And enter the extensions 
And enter the department
And choose prefferd currency
And enter the number of employees
And enter the ticker symbol
And enter web url
And enter the address line 1
And enter the city
And choose state
And enter the zip postal code
And enter the phone number
And enter the email address
