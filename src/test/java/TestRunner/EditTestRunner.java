package TestRunner;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.testng.AbstractTestNGCucumberTests;
@CucumberOptions(features = "src/test/java/LearnFeatures/EditLead.feature",
                 glue="Steps",dryRun=false,snippets=SnippetType.CAMELCASE,
                 monochrome=true)

public class EditTestRunner extends AbstractTestNGCucumberTests {

}
