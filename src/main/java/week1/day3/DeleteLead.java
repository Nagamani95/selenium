package week1.day3;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import week3.day2.ProjectMethods;

public class DeleteLead extends ProjectMethods {
	@BeforeTest
	public void setData() {
		excelFileName="DeleteLead";
	}
    @Test(dataProvider="fetchData")

	public  void DL(String cn, String fn, String ln)throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main\r\n");
		driver.manage().window().maximize();
		WebElement username = driver.findElementById("username");
		username.sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("//*[@id='ext-gen346']/ul/li[2]/a[2]/em/span[1]").click();
		driver.findElementByName("phoneNumber").sendKeys("8870430819");
		driver.findElementByLinkText("Find Leads").click();
		Thread.sleep(3000);
		driver.findElementByXPath("//*[@id='ext-gen472']/div/div/table/tbody/tr/td/div/a").click();
		driver.findElementByClassName("subMenuButtonDangerous").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByName("id").sendKeys("10077");
		driver.findElementByXPath("//*[@id='ext-gen334']").click();
		Thread.sleep(3000);
		driver.close();
		

	}

}
