package week1.day3;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import week3.day2.ProjectMethods;

public class EditLead extends ProjectMethods {
	@BeforeTest
	public void setData() {
		excelFileName="EditLead";
	}
    @Test(dataProvider="fetchData")

	
	public  void EL(String cn, String fn, String ln) throws InterruptedException {
		@SuppressWarnings("unused")
		String vName="Lead";
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main\r\n");
		driver.manage().window().maximize();
		WebElement username = driver.findElementById("username");
		username.sendKeys("DemoCSR");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("//*[@id='ext-gen248']").sendKeys("jagan");
		driver.findElementByXPath("//*[@id='ext-gen334']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-firstName']//a[1]").click();
		String vName1 = driver.findElementById("sectionHeaderTitle_leads").getText();
		if(vName1.equals(vName1)) {
			System.out.println("sucessfully verified");
		}else
		{
			System.out.println("not verified");
		}
		driver.findElementByXPath("//*[@id='center-content-column']/div[2]/div[1]/div[1]/div/div/div/div[2]/a[3]").click();
		driver.findElementById("updateLeadForm_companyName").clear();
		driver.findElementById("updateLeadForm_companyName").sendKeys("Tata");
		driver.findElementByClassName("smallSubmit").click();
		Thread.sleep(2000);
		driver.close();
		
	}

}
