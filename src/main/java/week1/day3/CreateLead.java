package week1.day3;

import java.util.List;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import week3.day2.ProjectMethods;

public class CreateLead extends ProjectMethods {
	@BeforeTest
	public void setData() {
		excelFileName="Login";
	}
	@Test(dataProvider="fetchData")
	
	public  void CL(String cn, String fn, String ln) {
		 System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main\r\n");
		driver.manage().window().maximize();
		WebElement username = driver.findElementById("username");
		username.sendKeys("DemoCSR");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Create Lead").click();
		String name ="tnpl";
		WebElement cName = driver.findElementById("createLeadForm_companyName");
		cName.sendKeys(name);
		//driver.findElementById("createLeadForm_companyName").sendKeys("tnpl");
		driver.findElementById("createLeadForm_firstName").sendKeys("naga");
		driver.findElementById("createLeadForm_lastName").sendKeys("mani");
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("sel");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("ars");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("D");
		WebElement eleSource = driver.findElementById("createLeadForm_dataSourceId");
		Select eleDropDown = new Select(eleSource);
		eleDropDown .selectByVisibleText ("Employee");
		WebElement eleSource1 = driver.findElementById("createLeadForm_marketingCampaignId");
		Select eleDropDown1 =new Select(eleSource1);
		eleDropDown1.selectByValue("DEMO_MKTG_CAMP");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Engineering");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("178000");
		WebElement eleSource2 = driver.findElementById("createLeadForm_industryEnumId");
		Select eleDropDown2 =new Select(eleSource2);
		eleDropDown2 .selectByIndex(3);
		WebElement eleSource3 = driver.findElementById("createLeadForm_ownershipEnumId");
		Select eleDropDown3 = new Select(eleSource3);
		eleDropDown3.selectByVisibleText("Corporation");
		driver.findElementById("createLeadForm_sicCode").sendKeys("639113");
		driver.findElementById("createLeadForm_description").sendKeys("all the employees are asked to atend the meeting without fail");
		driver.findElementById("createLeadForm_importantNote").sendKeys("every one should attend without fail");
		driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("600100");
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("04324");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("271361");
		driver.findElementById("createLeadForm_departmentName").sendKeys("informationtech");
		WebElement eleSource4 = driver.findElementById("createLeadForm_currencyUomId");
		Select eleDropDown4 =new Select(eleSource4);
		List<WebElement> allOptions = eleDropDown4.getOptions();
		int size = allOptions.size();
        eleDropDown4.selectByIndex(size-1);
        driver.findElementById("createLeadForm_numberEmployees").sendKeys("450");
        driver.findElementById("createLeadForm_tickerSymbol").sendKeys("@#");
        driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("http://");
        driver.findElementById("createLeadForm_generalToName").sendKeys("nagamani");
        driver.findElementById("createLeadForm_generalAddress1").sendKeys("24/A");
        driver.findElementById("createLeadForm_generalAddress2").sendKeys("pasupathinagar pugalur");
        driver.findElementById("createLeadForm_generalCity").sendKeys("karur");
        WebElement eleSource5 = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
        Select eleDropDown5 = new Select(eleSource5);
       eleDropDown5.selectByValue("FL");
        driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("8870430819");
        driver.findElementById("createLeadForm_primaryEmail").sendKeys("mani.naga89@gmail.com");
        driver.findElementById("createLeadForm_generalPostalCode").sendKeys("639113");
        driver.findElementByClassName("smallSubmit").click();
        String vName = driver.findElementById("viewLead_companyName_sp").getText();
        if(name.equals(vName)) {
         System.out.println("matched");
        }else
        {
        	System.out.println("not matched");
        }
        driver.close();
	}

}
