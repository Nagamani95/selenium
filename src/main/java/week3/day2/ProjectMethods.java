package week3.day2;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import week4.day1.dataFetcher;

public class ProjectMethods {

	public ChromeDriver driver;
	public String excelFileName;
	@DataProvider(name="fetchData")
	public  String[][] getData()throws IOException {
		return dataFetcher.readExcel(excelFileName);
	}
	@Parameters({"url","username","password"})
  @BeforeMethod
	public void startApp(String url, String uName, String pwd)
	{
		 System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		    driver = new ChromeDriver();
			driver.get(url);
			driver.findElement(By.id("username")).sendKeys(uName);
			driver.findElement(By.id("password")).sendKeys(pwd);
			driver.findElement(By.xpath("//input[@class='decorativeSubmit']")).click();
			driver.findElement(By.linkText("CRM/SFA")).click();
			driver.findElement(By.linkText("Leads")).click();

	}
	@AfterMethod
	public void closeApp()
	{
		driver.close();
	}
	
}
