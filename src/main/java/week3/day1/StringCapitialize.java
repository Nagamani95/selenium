package week3.day1;

public class StringCapitialize {

	public static void main(String[] args) {
		String capitialize="capitialize first letter of the words";
		System.out.println("\n2.string to be modified -"+capitialize+"\n");
		String[] words=capitialize.split(" ");
		for (String string : words) {
			char[] cs=string.toCharArray();
			System.out.print(Character.toUpperCase(cs[0]));
			for(int i=1;i<cs.length;i++)
			{
				System.out.print(cs[i]);
				System.out.print(" ");
			}
			System.out.print(" ");
		}
	}

}
