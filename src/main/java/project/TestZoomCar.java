package project;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestZoomCar {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.zoomcar.com/chennai");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//a[@class='search']")).click();
		driver.findElement(By.xpath("//div[text()[normalize-space()='Thuraipakkam']]")).click();
		driver.findElement(By.xpath("//button[text()='Next']")).click();
		driver.findElement(By.xpath("//div[text()='Sat']")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[@class='proceed']")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[text()='Done']")).click();
		
	}

}
