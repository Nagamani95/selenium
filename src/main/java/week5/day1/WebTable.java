package week5.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebTable {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
	 driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://erail.in/");
		WebElement src = driver.findElementById("txtStationFrom");
		src.clear();
		src.sendKeys("MAS",Keys.TAB);
		driver.findElementById("chkSelectDateOnly").click();
		WebElement to = driver.findElementById("txtStationTo");
		to.clear();
		to.sendKeys("PDY",Keys.TAB);
		WebElement table = driver.findElementByXPath("//table[@class='DataTable DataTableHeader TrainList']");
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		for (WebElement row : rows) {
			List<WebElement> tds = row.findElements(By.tagName("td"));
			System.out.println(tds.get(0).getText());
			}
		}
		
}


