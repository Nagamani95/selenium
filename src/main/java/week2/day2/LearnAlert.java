package week2.day2;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnAlert {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leafground.com/pages/Alert.html");
		driver.manage().window().maximize();
		driver.findElementByXPath("//button[text()='Prompt Box']").click();
		Alert alert = driver.switchTo().alert();
		@SuppressWarnings("unused")
		String text = alert.getText();
		System.out.println("text");
		alert.sendKeys("testlesf chennai");
		alert.accept();

	}

}
