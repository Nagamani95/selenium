package week2.day2;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FunWithFrame {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leafground.com/pages/frame.html");
		driver.switchTo().frame(0);
		WebElement button = driver.findElementById("Click");
		System.out.println("");
		System.out.println(button.getText());
		button.click();
		System.out.println(button.getText());
		System.out.println("");
		driver.switchTo().defaultContent();
		driver.switchTo().frame(1);
		driver.switchTo().frame("frame2");
		WebElement button2 = driver.findElementById("Click1");
		System.out.println("");
		System.out.println(button2.getText());
		button2.click();
		System.out.println(button2.getText());
		System.out.println("");
		driver.switchTo().defaultContent();
		driver.switchTo().frame(2);
		driver.switchTo().frame("frame2");
		WebElement element = driver.findElementByTagName("body");
		@SuppressWarnings("unused")
		String bodyText = element.getText();
		System.out.println("bodyText");
		System.out.println();
		System.out.println("3 mainframe 2nestedframe total 5frames ");
		
		
		
	}

}
