package week2.day4;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class WindowHandleAndAction {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.indeed.co.in/freshers-jobs");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElementByXPath("//div[@class='title']/a[1]").click();
		Actions builder = new Actions(driver);
		builder.keyDown(Keys.CONTROL).click().perform();
	    Set<String> allWindow =driver.getWindowHandles();
	    System.out.println(driver.getTitle());
		List<String> listOfWindows = new ArrayList<String>();
		listOfWindows.addAll(allWindow);
		driver.switchTo().window(listOfWindows.get(0));

	}

}
