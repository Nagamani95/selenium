package week2.day3;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class MergeCase {
	@Test(timeOut =20000)
	
	public void MC ()throws InterruptedException {
		 {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			ChromeDriver driver = new ChromeDriver();
			driver.get("http://leaftaps.com/opentaps/control/main\r\n");
			driver.manage().window().maximize();
			WebElement username = driver.findElementById("username");
			username.sendKeys("DemoSalesManager");
			driver.findElementById("password").sendKeys("crmsfa");
			driver.findElementByClassName("decorativeSubmit").click();
			driver.findElementByLinkText("CRM/SFA").click();
			driver.findElementByLinkText("Leads").click();
			driver.findElementByLinkText("Merge Leads").click();
			Thread.sleep(3000);
		    driver.findElementByXPath("//td[@class='titleCell']//following::img").click();
		    Set<String> allWindow =driver.getWindowHandles();
			System.out.println(driver.getTitle());
			List<String> listOfWindows = new ArrayList<String>();
			listOfWindows.addAll(allWindow);
			driver.switchTo().window(listOfWindows.get(1));
			driver.findElementByXPath("//label[text()='Lead ID:']//following::input[1]").sendKeys("10233");
			driver.findElementByXPath("//button[text()='Find Leads']").click();
			Thread.sleep(2000);
			driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']//a").click();
			Thread.sleep(3000);
			driver.switchTo().window(listOfWindows.get(0));
			driver.findElementByXPath("//input[@name='partyIdTo']//following::a[1]").click();
			 Set<String> allWindow1 =driver.getWindowHandles();
			System.out.println(driver.getTitle());
			List<String> listOfWindows1 = new ArrayList<String>();
			listOfWindows1.addAll(allWindow1);
			driver.switchTo().window(listOfWindows1.get(1));
			Thread.sleep(3000);
			driver.findElementByXPath("//label[text()='Lead ID:']//following::input[1]").sendKeys("10240");
			driver.findElementByXPath("//button[text()='Find Leads']").click();
			Thread.sleep(3000);
           driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']//a").click();
            Thread.sleep(2000);
            driver.switchTo().window(listOfWindows1.get(0));
		    driver.findElementByXPath("//a[@class='buttonDangerous']").click();
		    Alert alert = driver.switchTo().alert();
		    alert.accept();
			driver.findElementByLinkText("Find Leads").click();
			driver.findElementByXPath("//label[text()='Lead ID:']//following::input[1]").sendKeys("10233");
			driver.findElementByXPath("//button[text()='Find Leads']").click();
			Thread.sleep(6000);
			driver.close();

	}
	}
}


