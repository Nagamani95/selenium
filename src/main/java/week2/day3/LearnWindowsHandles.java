package week2.day3;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindowsHandles {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().window().maximize();
		driver.findElementByLinkText("Contact Us").click();
		Set<String> allWindow =driver.getWindowHandles();
		System.out.println(driver.getTitle());
		List<String> listOfWindows = new ArrayList<String>();
		listOfWindows.addAll(allWindow);
		driver.switchTo().window(listOfWindows.get(1));
		String text = driver.findElementByXPath("/html/body/div/div[2]/b/p[2]").getText();
		System.out.println(text);
		

	}

}
